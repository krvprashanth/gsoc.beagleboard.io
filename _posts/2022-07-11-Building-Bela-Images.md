---
layout: post
title: 'Building Bela Images'
---


### Building Bela Image Project
- _Student Name:_ Kurva Prashanth
- _Mentors:_ Giulio Moro, Vedant Paranjape, Vaishnav Achath

### Helpful Links 
- [Proposal Wiki](https://elinux.org/BeagleBoard/GSoC/2022_Proposal/Building_Bela_Images)
- [Repository](https://git.beagleboard.org/gsoc/building-bela-images)
- [Weekly Report](https://krvprashanth.in/gsoc2022/)
- [Introductory Video](https://youtu.be/JESc32I59TQ)
