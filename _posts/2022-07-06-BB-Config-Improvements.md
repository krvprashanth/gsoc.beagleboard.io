---
layout: post
title: 'BB-config improvements & GPIO Benchmarking'
---

BB-config improvements & GPIO Benchmarking project:
Student: Jian De
Mentors: Shreyas Atre, Vedant Paranjape, Vaishnav Achath

[Proposal](https://elinux.org/BeagleBoard/GSoC/2022_Proposal/bb-config_improvements_%26_GPIO_Benchmarking)
[Repository](https://git.beagleboard.org/gsoc/bb-config)
[Weekly Report](https://gsoc.beagleboard.io/bb-config/)
[Introductory Video](https://youtu.be/V_Euk5uWY1o)
